import configparser


class User:

    def __init__(self, username, password):
        self.username = username
        self.password = password 

    def connect(self):
        # TODO: Implement
        pass


def read_credentials():
    """This function reads the config.ini file
    and return an instance of User with the 
    username and password available in the file.
    """
    config = configparser.ConfigParser()
    config.read('config.ini')
    username = config['credentials']['username']
    password = config['credentials']['password']
    return User(username, password)

def sum(a, b):
    """Sum a and b"""
    return a+b

def multiply(a, b):
    """Multiply a and b"""
    return a*b

def print_upper(text):
    """Print a given string in upper case."""
    print(str(text).capitalize())
